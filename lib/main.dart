import 'package:belajar_flutter/src/screens/auth/auth_screen.dart';
import 'package:belajar_flutter/src/screens/auth/auth_service.dart';
import 'package:belajar_flutter/src/screens/counter/counter_screen.dart';
import 'package:belajar_flutter/src/screens/counter/counter_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<CounterService>(
          create: (_) => CounterService(),
        ),
        ChangeNotifierProvider<AuthService>(
          create: (_) => AuthService(),
        ),
      ],
      builder: (context, child) => MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: CounterScreen(),
      ),
    );
  }
}
