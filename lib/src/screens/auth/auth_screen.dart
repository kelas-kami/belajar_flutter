import 'package:belajar_flutter/src/screens/auth/auth_service.dart';
import 'package:belajar_flutter/src/widgets/my_button.dart';
import 'package:belajar_flutter/src/screens/auth/widgets/auth_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: FocusScope.of(context).unfocus,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: DecoratedBox(
            decoration: const BoxDecoration(
              color: Color(0xFFE5ECF4),
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(8),
                bottomRight: Radius.circular(8),
              ),
            ),
            child: GestureDetector(
              child: const Icon(
                Icons.keyboard_arrow_left,
                size: 32,
                color: Color(0xFF1F4D79),
              ),
              onTap: () => Navigator.pop(context),
              // onTap: () => print('something'),
            ),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Hello Again!\nWelcome\nback',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFF1F4D79),
                ),
              ),
              const SizedBox(height: 16),
              AuthTextField(
                label: 'Email Address',
                onChange: context.read<AuthService>().onEmailChange,
              ),
              const SizedBox(height: 16),
              AuthTextField(
                label: 'Password',
                isObfucated: true,
                onChange: context.read<AuthService>().onPasswordChange,
              ),
              const SizedBox(height: 16),
              MyButton(
                onPress: context.read<AuthService>().login,
                label: 'Sign In',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
