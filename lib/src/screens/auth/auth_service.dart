import 'package:flutter/foundation.dart';

class AuthService extends ChangeNotifier {
  bool _isLoading;
  bool get isLoading => _isLoading;

  void setLoading() => _isLoading = true;
  void setFinishLoading() => _isLoading = false;

  String _email;
  String get email => _email;

  String _password;
  String get pasword => _password;

  Future<void> login() async {
    setLoading();
    notifyListeners();

    print('em: $_email pw: $_password');

    await Future.delayed(Duration(milliseconds: 1000));

    setFinishLoading();
    notifyListeners();
  }

  void onEmailChange(String value) {
    _email = value;
  }

  void onPasswordChange(String value) {
    _password = value;
  }
}
