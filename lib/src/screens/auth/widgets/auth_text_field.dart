import 'package:flutter/material.dart';

class AuthTextField extends StatefulWidget {
  const AuthTextField({
    @required this.label,
    this.onChange,
    this.isObfucated,
  });

  final String label;
  final void Function(String) onChange;
  final bool isObfucated;

  @override
  _AuthTextFieldState createState() => _AuthTextFieldState();
}

class _AuthTextFieldState extends State<AuthTextField> {
  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: const Color(0xFFFFFFFF),
        borderRadius: BorderRadius.circular(16),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: TextField(
          decoration: InputDecoration(
            labelText: widget.label,
            labelStyle: const TextStyle(color: Color(0xFF1F4D79)),
            border: InputBorder.none,
          ),
          onChanged: widget.onChange,
          cursorColor: const Color(0xFF1F4D79),
          obscureText: widget.isObfucated ?? false,
          style: const TextStyle(
            color: Color(0xFF1F4D79),
          ),
        ),
      ),
    );
  }
}
