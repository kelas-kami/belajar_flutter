import 'package:belajar_flutter/src/screens/counter/counter_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CounterScreen extends StatefulWidget {
  @override
  _CounterScreenState createState() => _CounterScreenState();
}

class _CounterScreenState extends State<CounterScreen> {
  @override
  Widget build(BuildContext context) {
    //
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Consumer<CounterService>(
                builder: (_, CounterService value, __) =>
                    Text('${value.counter}'),
              ),
              TextButton(
                onPressed: context.read<CounterService>().increment,
                child: Text('Increment'),
              ),
              TextButton(
                onPressed: context.read<CounterService>().decrement,
                child: Text('Decrement'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
